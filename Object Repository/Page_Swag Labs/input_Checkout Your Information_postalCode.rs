<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Checkout Your Information_postalCode</name>
   <tag></tag>
   <elementGuidId>3fae0f39-f3b1-4415-8f5f-ad2b5cf95428</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#postal-code</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='postal-code']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>27f912e0-8cdf-493b-8d48-43acb7714bd7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>input_error form_input</value>
      <webElementGuid>829d1b99-3f4b-4d8c-8c66-52c54eb10f57</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Zip/Postal Code</value>
      <webElementGuid>29d29a77-1477-4ab0-a2d8-9e6e10402053</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>fbbf51fa-c452-4019-b347-ac426665b425</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-test</name>
      <type>Main</type>
      <value>postalCode</value>
      <webElementGuid>76167eab-c9d7-421b-96cb-f3bb3762a721</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>postal-code</value>
      <webElementGuid>db88f779-ad8b-4aa8-b7f9-5522da42fecb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>postalCode</value>
      <webElementGuid>8307c726-e88f-453b-a5b1-89469a032665</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocorrect</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>6e6ecb38-df53-4283-9422-f3fc0377a6e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocapitalize</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>8623f028-40fe-4a39-9c33-e41188515764</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;postal-code&quot;)</value>
      <webElementGuid>d2f0f4e9-db95-4206-b21c-e0870f377221</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='postal-code']</value>
      <webElementGuid>048929df-03d6-4729-8c11-91386b17e737</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout_info_container']/div/form/div/div[3]/input</value>
      <webElementGuid>62972388-5a40-4fdb-bf36-08b2b96c7afc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/input</value>
      <webElementGuid>f4e3013e-a76c-430b-9287-5285edec1ea1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'Zip/Postal Code' and @type = 'text' and @id = 'postal-code' and @name = 'postalCode']</value>
      <webElementGuid>c299d578-134e-48ce-be43-6b9dbcd8a28f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
