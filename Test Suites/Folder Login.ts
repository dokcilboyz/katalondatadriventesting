<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Folder Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>7ef24613-fef6-43a5-898b-f5375cee120f</testSuiteGuid>
   <testCaseLink>
      <guid>8bc68d49-f163-4735-9b4a-9aadbd21b0f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Data Driven Testing/TC01 - Login Standar User</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>b26a0af1-865f-42e1-807a-6a19de27bbc8</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Data Login</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>b26a0af1-865f-42e1-807a-6a19de27bbc8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Login</value>
         <variableId>efe9f9e4-556a-4150-b705-28ce33db583c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>b26a0af1-865f-42e1-807a-6a19de27bbc8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>ac411e16-1709-40f5-bcc8-0093492cab6d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>97c74c6c-18ed-4d87-8483-9ce0ea624189</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Data Driven Testing/TC03 - Login Locked User</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>0bb6f622-6941-4bc0-94fc-b5feccf1d311</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Data Driven Testing/TC04 - Login Problem User</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5af317d8-af9c-4f0f-971d-1c6d26dcc047</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Data Driven Testing/TC05 - Login Glitch User</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4c8d18dd-0a24-4647-a0ca-307b4d475f87</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Data Driven Testing/TC02 - Logout</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
